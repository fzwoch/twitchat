# TwitChat

A simple console reader for Twitch.tv chats.

## Usage

```shell
twitchat <channel>
```

## Screenshot

![Screenshot](twitchat.png "TwitChat screenshot")
