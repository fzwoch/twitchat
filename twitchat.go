package main

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"math/rand"
	"os"
	"runtime"
	"strconv"
	"strings"
	"time"

	irc "github.com/thoj/go-ircevent"
)

const (
	resetCode = "\x1b[0m"
	grayCode  = "\x1b[38;2;168;168;168m"
)

var N []int

func init() {
	for i, n := range []int{47, 68, 40, 40, 40, 21} {
		for f := 0; f < n; f++ {
			N = append(N, i)
		}
	}
}

func getColorCode(s string) string {
	var r, g, b int

	n, _ := fmt.Sscanf(s, "#%02x%02x%02x", &r, &g, &b)

	if n != 3 {
		return grayCode
	}

	if runtime.GOOS == "darwin" {
		return fmt.Sprintf("\x1b[38;5;%dm", 16+36*N[r]+6*N[g]+N[b])
	} else {
		return fmt.Sprintf("\x1b[38;2;%d;%d;%dm", r, g, b)
	}
}

func getTimeCode() string {
	return grayCode + time.Now().Format("15:04") + resetCode
}

func main() {
	if len(os.Args) != 2 {
		fmt.Println("Usage:", os.Args[0], "<channel>")
		return
	}

	login := struct {
		UserName   string `json:"username"`
		OAuthToken string `json:"token"`
	}{
		UserName: "justinfan" + strconv.Itoa(rand.Intn(900)+100),
	}

	buf, err := os.ReadFile("twitchat.json")
	if err == nil {
		json.Unmarshal(buf, &login)
	}

	var timer *time.Timer

	irccon := irc.IRC(login.UserName, login.UserName)
	irccon.UseTLS = true
	irccon.TLSConfig = &tls.Config{
		InsecureSkipVerify: true,
	}
	irccon.RequestCaps = []string{"twitch.tv/tags", "twitch.tv/membership", "twitch.tv/commands"}
	irccon.Password = login.OAuthToken

	irccon.AddCallback("001", func(e *irc.Event) {
		timer = time.AfterFunc(2*time.Second, func() {
			fmt.Println(getTimeCode() + " " + "JOIN timed out")
			irccon.Quit()
		})
		irccon.Join("#" + strings.ToLower(os.Args[1]))
	})

	irccon.AddCallback("JOIN", func(e *irc.Event) {
		if e.Nick == irccon.GetNick() {
			timer.Stop()
			fmt.Println(getTimeCode() + " " + "- Joined -")
		}
	})

	irccon.AddCallback("PRIVMSG", func(e *irc.Event) {
		if len(e.Tags["display-name"]) > 16 {
			e.Tags["display-name"] = e.Tags["display-name"][:14] + ".."
		}
		name := fmt.Sprintf("%16s", e.Tags["display-name"])
		fmt.Println(getColorCode(e.Tags["color"]) + name + resetCode + " " + getTimeCode() + " " + e.Arguments[1])
	})

	irccon.AddCallback("NOTICE", func(e *irc.Event) {
		fmt.Println(getTimeCode() + " " + e.Arguments[1])

		if e.Tags["msg-id"] == "msg_channel_suspended" {
			irccon.Quit()
		}
	})

	err = irccon.Connect("irc.chat.twitch.tv:6697")
	if err != nil {
		panic(err)
	}

	irccon.Loop()
}
